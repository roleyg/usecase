import java.util.Scanner;

public class multiply {

    public static void main(String[] args) {

        /* Read user input
         */
        Scanner scan = new Scanner(System.in);
        System.out.print("Enter first number: ");

        // Reads first number provided by the user
        int num1 = scan.nextInt();

        //Reads second number provided by the user
        System.out.print("Enter second number: ");
        int num2 = scan.nextInt();

        //Close
        scan.close();

        // Multiplies the 2 numbers together
        int product = num1*num2;

        // Displays the results 
        System.out.println("Output: "+product);
    }
}